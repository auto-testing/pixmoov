describe("Пиксмув", () => {
    beforeEach(() => {  
    cy.visit("https://forhemer.github.io/React-Todo-List")
    .get(".input-text")
    .type("1")
    .get(".input-submit")
    .click()
    .get(".input-text")
    .type("2")
    .get(".input-submit")
    .click()
    .get(".input-text")
    .type("3")
    .get(".input-submit")
    .click()
    })
    it("first-test", () => {
        cy.get(".TodoItem_item__iFWQW")
        .should("have.length", 3)
    })
    it("second-test", () => {
       cy.get(".TodoItem_checkbox__Tf0FQ").eq(0)
       .click()
       .get('[style*="line-through"]')
       .should('be.visible')
    })
    it("third-test", () => {
        let secondElement
        cy.get("span").eq(1)
        .invoke('text')
        .then((text) => {
            secondElement = text
        })
        .get('[type = "button"]').eq(1)
        .click()
        .get('span').eq(1)
        .invoke('text')
        .then(el => {
            expect(el).not.equals(secondElement)
        })
    })
})